import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
 titulo = "NOA MOTORS";

 

  cards = [ 
    {
    titulo: "Kawasaki Ninja 300", 
    subtitulo: "R$ 19.990,00",
    conteudo: "Ela sai com determinação e ao passar pela faixa do “veneno” (de 8.000 aos 12.000 rpm) ela acelera com força, como antes e até um pouco mais. Não precisa mais baixar marchas para uma simples ultrapassagem.",
    foto: "https://www.motonline.com.br/storage/guides/kawasaki/ninja-300.jpg"
  },
  {
    titulo: "Ducati Street Fighter V4 S ",
    subtitulo : "R$ 146.990,00",
    conteudo : "Essa máquina possui um guidão mais largo e mais pesado, assim, pesa cerca de 178 kg, equipada por um Desmosedici Stradale de 1.100 cc. Finalmente, o modelo entrega 208 cv altamente controláveis graças às asas laterais, aliadas a um pacote eletrônico de última geração.",
    foto: "https://reviewauto.com.br/wp-content/uploads/2021/12/Ducati-1299-Panigale.jpg"
  },
  {
    titulo: "Kawasaki Ninja H2R",
    subtitulo : "R$ 168.000,00",
    conteudo : "A Ninja H2R conta com um motor com uma potência absurda de 300 cavalos. Este, por sua vez, conta ainda com o auxílio do supercharger, cuja hélice possui a capacidade de bombear mais de 200 litros por segundo.",
    foto: "https://reviewauto.com.br/wp-content/uploads/2021/12/Kawasaki-Ninja-H2R.jpg"
  },
  {
    titulo: "Yamaha R1",
    subtitulo : "R$ 52.013,00",
    conteudo : "Um dos grandes diferenciais do modelo vencedor da categoria transporte do Design Awards, aclamado concurso alemão, foi o conceito “Speed Racer”, criado para proporcionar a pilotos comuns uma experiência completa dos conceitos técnicos e tecnologias de uma máquina de MotoGP.",
    foto: "https://reviewauto.com.br/wp-content/uploads/2021/12/Yamaha-R1.jpg"
  }
  ];
 
  constructor() {}

}